# Intel 8080 Assembly Reference Pamphlet

In 1976, to go along with the development tools and documentation for the 8080, Intel printed a quick reference pamphlet which has every single instruction and opcode, as well as a hex-ASCII table and pseudo instructions for their assembler. It also included a list of Intel offices around the world at the time. The pamphlet is clearly targeted at users of their MCS-80 development system, but it's still useful for anyone programming the 8080.

The available scan of this pamphlet is fairly decent, but the original document itself had stains and blemishes as well as writing on it from a previous owner. I wanted a clean, vector version of the document so I could print out a new copy. This project is an undertaking to create that vector copy.

I am not an expert in document recreation/archiving. I drew inspiration from Professor David Brailsford of Nottingham University -- he has recreated a few documents, including the famous "Summer Vacation" memo from Bell Labs, as well as Dennis Ritchie's thesis. I do have experience working with vector and bitmap images, but I'm not an artist, and this is definitely going to be a test of my knowledge so far. I've already learned a lot more about Inkscape, and also how difficult it can be to identify colours from old, scanned documents (just what *is* the colour Intel used for their Intel blue on this document?).

I've only just begun the first page as I'm writing this, and hopefully I will slowly chip away at it until it's done. The second page (other side of the pamphlet) will likely be more difficult to get exactly correct, but I'll do my best.

### Contributions

Contributions are welcome, but please contact me first before doing a whole bunch of the work. This is partially a learning experience for me and if someone just comes along and finishes the project, that will be less useful to me. However, corrections to the work I've done, as well as anyone who can contribute a better quality source scan would be greatly appreciated!

### Copyright

The original copyright on the document is clearly still held by Intel. It was published by someone in 1976, so presumably the copyright won't expire for at least another 20 or so years. However, I'm just hoping that nobody cares enough about a document that is almost useless today, besides the small niche of people still playing with 8080-based systems.

### License

Because of the ambiguous legal situation, I've decided to put *my work* under CC BY-NC-ND -- attribution, non-commercial, no derivatives. I rarely use this combination, but because I definitely cannot give permission to create derivative works, I figured this was the best middle ground. You can basically print this out and share it with people, but you can't use it commercially (which would probably annoy Intel) and you can't create derivative works. See [LICENSE.txt] for details.